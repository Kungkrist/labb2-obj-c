//
//  RandomQuestionGenerator.m
//  traning2
//
//  Created by DEA on 2016-02-02.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "RandomQuestionGenerator.h"
#import "Question.h"

@interface RandomQuestionGenerator ()

@property (nonatomic) NSArray *questions;
@property (nonatomic) Question *currentQuestion;
@property (nonatomic) NSMutableSet *usedQuestions;
@end

@implementation RandomQuestionGenerator

// Setting up the constructor
- (instancetype) init {

    self = [super init];
    
    if(self) {
        int maxGames = self.maxAmountOfGames;
        self.maxAmountOfGames = maxGames == 0 ? 5 : maxGames;
        [self setUp];
    }
    
    return self;
}

// Sets up the game by creating questions and puts them in a NSArray
// Also sets som standard values that are needed for a new game.
-(void) setUp {
    NSDictionary *answers1 = @{@"[Classname init]" : @"0",
                               @"[Classname alloc]" : @"0",
                               @"... = new Classname()" : @"0",
                               @"[[Classname alloc] init]" : @"1"};
    
    NSDictionary *answers2 = @{@"instancetype" : @"1",
                               @"self" : @"0",
                               @"the class name" : @"0",
                               @"this" : @"0"};
    
    NSDictionary *answers3 = @{@"You just put it in the .h file" : @"0",
                               @"You put a + instead of a -" : @"1",
                               @"You write static instead of a -" : @"0",
                               @"You can't create a static method" : @"0"};
    
    NSDictionary *answers4 = @{@"#property () Type name" : @"0",
                               @"@property () name" : @"0",
                               @"@property () Type name" : @"1",
                               @"#property () name" : @"0"};
    
    NSDictionary *answers5 = @{@"NextStep" : @"1",
                               @"NextStunt" : @"0",
                               @"NotSafe" : @"0",
                               @"Nothing" : @"0"};
    
    NSDictionary *answers6 = @{@"1960s" : @"0",
                               @"1970s" : @"0",
                               @"1980s" : @"1",
                               @"1990s" : @"0"};
    
    NSDictionary *answers7 = @{@"property-name" : @"0",
                               @"this.property-name" : @"0",
                               @"self.property-name" : @"1",
                               @"super.property-name" : @"0"};
    
    NSDictionary *answers8 = @{@"Can be found outside of class" : @"0",
                               @"Cannot be found outside of class" : @"0",
                               @"Can be modified after it is created" : @"0",
                               @"Cannot be modified after it is created" : @"1"};
    
    NSDictionary *answers9 = @{@"Nothing" : @"1",
                               @"The program casts an exception" : @"0",
                               @"The program gets slower" : @"0",
                               @"The program just crashes" : @"0"};
    
    NSDictionary *answers10 = @{@"%n" : @"0",
                                @"%@" : @"0",
                                @"%d" : @"1",
                                @"%s" : @"0"};
    
    Question *question1 = [[Question alloc]
                           initWith:@"How do you instantiate a class object?" and:answers1];
    Question *question2 = [[Question alloc]
                           initWith:@"What parameter type do you put in when creating a constructor?" and:answers2];
    Question *question3 = [[Question alloc]
                           initWith:@"How do you create a static method?" and:answers3];
    Question *question4 = [[Question alloc]
                           initWith:@"How do you write a property?" and:answers4];
    Question *question5 = [[Question alloc]
                           initWith:@"What does the NS in NSString stand for?" and:answers5];
    Question *question6 = [[Question alloc]
                           initWith:@"When was objective-c created?" and:answers6];
    Question *question7 = [[Question alloc]
                           initWith:@"how do you call a property in the same class?" and:answers7];
    Question *question8 = [[Question alloc]
                           initWith:@"What does immutable stand for??" and:answers8];
    Question *question9 = [[Question alloc]
                           initWith:@"If you're trying to set a value of a nil object, what happens?" and:answers9];
    Question *question10 = [[Question alloc]
                            initWith:@"If you're tyring to log an int, what %X does you put in?" and:answers10];

    self.questions = @[question1, question2, question3, question4, question5, question6, question7, question8, question9, question10];
    
    [self resetGame];
}

// Generates a random question from the questions-list
// If the user has played to the max amount of questions then the current question will be nil.
- (void) generateRandomQuestion {
    
    int index = arc4random() % self.questions.count;
    Question *q = self.questions[index];

    // If the question is not used before
    if(![self compareQuestion:q.question]) {
        self.currentQuestion = [self.questions objectAtIndex:index];
        
        [self.usedQuestions addObject:(NSString *)q.question];
        
   } else {
       
       // If the amount of used question is not the same as the max amount of games.
       if(self.usedQuestions.count != self.maxAmountOfGames) {
           [self generateRandomQuestion];
           
       }else {
           self.currentQuestion = nil;
           self.gameHasEnded = YES;
       }
    }
    
    // If the amount of used question is the same as the max amount of games +1.
    if(self.usedQuestions.count == self.maxAmountOfGames +1) {
        self.currentQuestion = nil;
        self.gameHasEnded = YES;
    }
}

// Returns the current question
-(NSString *)getCurrentQuestion {
    return self.currentQuestion.question;
}

// Returns all the current answers that are connected to the current question
-(NSDictionary *)getCurrentAnswers {
    return self.currentQuestion.answers;
}

// If the user guessed right, return YES, and increment amountOfRightGuesses.
// Otherwise return NO.
-(BOOL) guess: (int) tag {
    NSDictionary *a = [self getCurrentAnswers];
    NSArray *answerKeys = [a allKeys];
    
    if([[a objectForKey: answerKeys[tag -1]] isEqualToString:@"1"]) {
        self.amountOfRightGuesses++;
        return YES;
    }
    
    return NO;
}

// This will look if the question is already used before in a game.
-(BOOL)compareQuestion: (NSString *) question {
    if([self.usedQuestions containsObject:question])
        return YES;
        
    return NO;
}

// This will reset the game
-(void) resetGame {
    self.usedQuestions = [[NSMutableSet alloc]init];
    
    if(self.maxAmountOfGames > self.questions.count)
        self.maxAmountOfGames = self.questions.count;
    
    self.gameHasEnded = NO;
    self.amountOfRightGuesses = 0;

}

// This method overrides the setter for maxAmountOfGames
-(void) setMaxAmountOfGames:(int)maxAmountOfGames {
    if(maxAmountOfGames > self.questions.count) {
        _maxAmountOfGames = self.questions.count;
    } else {
        _maxAmountOfGames = maxAmountOfGames;
    }
}

@end

