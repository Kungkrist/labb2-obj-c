//
//  RandomQuestionGenerator.h
//  traning2
//
//  Created by DEA on 2016-02-02.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface RandomQuestionGenerator : NSObject
@property (nonatomic) int maxAmountOfGames;
@property (nonatomic) int amountOfRightGuesses;
@property (nonatomic) BOOL gameHasEnded;
-(void) generateRandomQuestion;
-(NSString *) getCurrentQuestion;
-(NSDictionary *) getCurrentAnswers;
-(BOOL) guess: (int) tag;
-(void) resetGame;
@end
