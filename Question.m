//
//  Question.m
//  traning2
//
//  Created by DEA on 2016-02-03.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "Question.h"

@implementation Question

// Setting up the constructor
-(instancetype) initWith:(NSString *) Question
                     and:(NSDictionary *) Answers{
    
    self = [super init];
    
    if(self){
        self.question = Question;
        self.answers = Answers;
    }
    
    return self;
}
@end


