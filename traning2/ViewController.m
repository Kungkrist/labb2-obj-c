//
//  ViewController.m
//  traning2
//
//  Created by DEA on 2016-02-02.
//  Copyright © 2016 DEA. All rights reserved.
//

#import "ViewController.h"
#import "RandomQuestionGenerator.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *amountOfRightGuesses;
@property (weak, nonatomic) IBOutlet UILabel *AmountOfQuestionsAnswered;
@property (weak, nonatomic) IBOutlet UILabel *amountOfRightGuessesText;
@property (weak, nonatomic) IBOutlet UILabel *amountOfQuestionsAnsweredText;
@property (weak, nonatomic) IBOutlet UITextView *question;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UIButton *playAgainButton;
@property (nonatomic) UIButton *currentButton;
@property (nonatomic) RandomQuestionGenerator *generator;
@property (nonatomic) UIColor *originalColor;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.generator = [[RandomQuestionGenerator alloc] init];
    
    // Set tags for the answer-buttons.
    self.button1.tag = 1;
    self.button2.tag = 2;
    self.button3.tag = 3;
    self.button4.tag = 4;
    
    // Save the original background color for the answer-buttons.
    self.originalColor = self.button1.backgroundColor;
    
    // Set the max amount of questions for each turn.
    self.generator.maxAmountOfGames = 5;
    
    // Start the game.
    [self setUpView];
}
- (IBAction)playAgainButton:(UIButton *)sender {
    
    // Reset the game.
    [self.generator resetGame];
    
    // Show the view for when the game has started.
    [self hideOrShowButtonsAndText];
    
    // Start the game.
    [self setUpView];
}

- (IBAction)answerClick:(UIButton *)sender {
    self.currentButton = sender;
   
    // If the user guessed right
    if([self.generator guess:sender.tag] == YES) {
        
        // Set the background color to green
        UIColor *gColor = [UIColor greenColor];
        sender.backgroundColor = gColor;
    } else {
        
        // Set the background color to red
        UIColor *rColor = [UIColor redColor];
        sender.backgroundColor = rColor;
    }
    
    // Freeze the buttons
    [self freezeOrUnfreezeButtons];
    
    // Setup and start a timer that will freeze the buttons for 0.5 sec
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(changeButtonsBackToNormal) userInfo:nil repeats:NO];
}

// Freeze or unfreeze the answer-buttons.
-(void) freezeOrUnfreezeButtons {
    self.button1.enabled = !self.button1.enabled;
    self.button2.enabled = !self.button2.enabled;
    self.button3.enabled = !self.button3.enabled;
    self.button4.enabled = !self.button4.enabled;
}

// Change the buttons back to their original form.
-(void)changeButtonsBackToNormal {
    self.currentButton.backgroundColor = self.originalColor;
    
    // Unfreeze the buttons
    [self freezeOrUnfreezeButtons];
    
    [self setUpView];
}

-(void) setUpView {
    [self.generator generateRandomQuestion];
    NSDictionary *x = self.generator.getCurrentAnswers;
    NSArray *currentAnswers = [x allKeys];
    NSString *currentQuestion = self.generator.getCurrentQuestion;
    
    // Set the button text
    [self.button1 setTitle:currentAnswers[0] forState:UIControlStateNormal];
    [self.button2 setTitle:currentAnswers[1] forState:UIControlStateNormal];
    [self.button3 setTitle:currentAnswers[2] forState:UIControlStateNormal];
    [self.button4 setTitle:currentAnswers[3] forState:UIControlStateNormal];
    
    // Set the question text
    self.question.text = currentQuestion;
    
    // Set the font size
    [self.question setFont:[UIFont systemFontOfSize:24]];
    
    // If the game has ended
    if(self.generator.gameHasEnded) {
        
        // Set the text to the amount of right guesses the user had.
        self.amountOfRightGuesses.text =
        [NSString stringWithFormat:@"%d", self.generator.amountOfRightGuesses];
        
        // Set the text to the amount of questions the user has answered.
        self.AmountOfQuestionsAnswered.text =
        [NSString stringWithFormat:@"%d", self.generator.maxAmountOfGames];
        
        // Show the view for when the game has ended.
        [self hideOrShowButtonsAndText];
    }
}

// Hide or show text and buttons depending on if the game has ended or not.
-(void) hideOrShowButtonsAndText {
    self.question.hidden = !self.question.hidden;
    self.button1.hidden = !self.button1.hidden;
    self.button2.hidden = !self.button2.hidden;
    self.button3.hidden = !self.button3.hidden;
    self.button4.hidden = !self.button4.hidden;
    self.AmountOfQuestionsAnswered.hidden = !self.AmountOfQuestionsAnswered.hidden;
    self.amountOfRightGuesses.hidden = !self.amountOfRightGuesses.hidden;
    self.playAgainButton.hidden = !self.playAgainButton.hidden;
    self.amountOfQuestionsAnsweredText.hidden = !self.amountOfQuestionsAnsweredText.hidden;
    self.amountOfRightGuessesText.hidden = !self.amountOfRightGuessesText.hidden;
}

@end
