//
//  Question.h
//  traning2
//
//  Created by DEA on 2016-02-03.
//  Copyright © 2016 DEA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject
@property NSString *question;
@property NSDictionary *answers;

-(instancetype) initWith:(NSString *) Question
                     and:(NSDictionary *) Answers;
@end
